<?php
require_once __DIR__ . '/init.php';
$errors = array();

    echo "<hr>";

    if(!isset($_FILES['theFile'])) {
        // redirect to front page.
        echo $twig->render('ErrorDisplay.php');
        exit();
    }


    if($_FILES['theFile']['size'] == 0){
       $errors['empty'] = 'No file selected!';
    }

    if($_FILES['theFile']['error'] == 1){
        $errors['internalError'] = 'An error occurred.Please try again!';
    }

    if($_FILES['theFile']['size'] > 1048576){
        $errors['tooLarge'] = 'Max upload size 1MB. Trying to upload: ' . $_FILES['theFile']['size'] . 'bytes file';
    }

    $allowedExtensions = getAllowedExtensionArray();
    $fileExtension = getFileExtension($_FILES['theFile']['name']);

    if(!in_array($fileExtension, $allowedExtensions)){
        $errors['notAllowedExtension'] = 'Incompatible file extension!';
    }

    if(!empty($errors)){
        echo $twig->render('ErrorDisplay.php', array(
           'errors'     => $errors
        ));
        exit();
    }

    //uploading HERE
    $fileName = 'DOV-' . rand() . $_FILES['theFile']['name'];

    if(isset($_GET['dirname']) && $_GET['dirname'] != 'uploads'){
        $dirName = htmlspecialchars($_GET['dirname'], ENT_QUOTES);
        $fullDestination = $GLOBALS['defaultPath'] . $dirName . '/' . $fileName;
    }else if (isset($_GET['dirname'])){
        $fullDestination = $GLOBALS['defaultPath'] . $fileName;
    }

    $upload = move_uploaded_file($_FILES['theFile']['tmp_name'], $fullDestination);

    if(!$upload){
        echo $twig->render('ErrorDisplay.php', array(
            'errors'    => 'Failed upload.Server problem'
        ));
        exit();
    }else if($upload){
        echo $twig->render('ErrorDisplay.php',array(
            'success'   => 'File was uploaded!'
        ));
        exit();
    }

?>
