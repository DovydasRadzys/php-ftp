<?php
require_once 'init.php';

    if(!isset($_POST['mkdir'])){
        header("location:index.php");
        exit();
    }

    if($_POST['mkdir'] == null){
        header("location:index.php?emptyCreateDirName=true");
        exit();
    }

    $input = htmlspecialchars($_POST['mkdir']);
    $input = trim($input);
    $input = strtolower($input);


    if(!checkIfDirectoryExists($input)){
        header("location:index.php?directoryExists=true");
        exit();
    }

    //create the directory HERE
    if(!mkdir($GLOBALS['defaultPath'].$input, 0700)){
        header("location:index.php?directoryCreationFailed=true");
    }else{
        header("location:index.php?directoryCreated=true");
    }
