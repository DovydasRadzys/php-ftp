<?php
require_once 'init.php';

function getFileExtension($file){
    //cannot add explode() and end() into one line as end() requires reference which explode() doesn't provide
    $allExtensions = explode('.', $file);
    $lastExtension = end($allExtensions);
    return $lastExtension;
}

function getAllowedExtensionArray(){
    $allowedExtensions = array(
        'jpg','jpeg', 'png', 'gif','pdf','ppt', 'doc', 'odt', 'pptx', 'docx', 'txt'
    );
    return $allowedExtensions;
}

function twigInit(){
    $loader = new Twig_Loader_Filesystem('views/');
    $twig   = new Twig_Environment($loader);
    return $twig;
}

function fileAndDirectoryCleanUp($array){
    $newArray = array();
    $j =0;
    for($i=0;$i<count($array);$i++){
        if($array[$i] != '.' && $array[$i] != '..'){
            $newArray[$j] = $array[$i];
            $j++;
        }
    }
    return $newArray;
}

function displayUploadForm($dirName){
    ?>
    <form method="POST" action="uploadHandle.php?dirname=<?php echo $dirName; ?>" enctype="multipart/form-data">
        <input type="file" name="theFile"><br/>
        <input type="submit" value="Upload">
    </form>
    <?php
}

function FileAndDirPrettyDisplay($data, $uploadDir){
    $size = count($data);
        for($i=0;$i<$size;$i++){
            $type = filetype($uploadDir . $data[$i]);
                if($type == 'dir'){
                    echo "<a href='index.php?dirname=$data[$i]'>" .$data[$i]. "</a></br>";
                }else if($type == 'file'){
                    $trailingDir = basename($uploadDir);
                    echo "<a href='download.php?filename=$data[$i]&dirname=$trailingDir'>" .$data[$i]. "</a></br>";
                }
        }
}

function readAllFilesAndDirectories($open){
    $filesAndDirectories = array();
    $i =0;
    while(($file = readdir($open)) !== FALSE){
        $filesAndDirectories[$i] = $file;
        $i++;
    }
    $filesAndDirectoriesCleanedUp = fileAndDirectoryCleanUp($filesAndDirectories);
    return $filesAndDirectoriesCleanedUp;
}

function scanDirectory($twig, $dirName){
    $errors = array();

        if(!is_dir($dirName)){
            $errors['isDirectory'] = 'Base upload directory is not a directory!';
        }

        if(!@$open = opendir($dirName)){
            $errors['canOpenDirectory'] = 'Not able to open the directory';
        }

        if(!empty($errors)){
            echo $twig->render('ErrorDisplay.php', array(
                    'errors'    => $errors
            ));
            die();
        }


        $filesAndDirectories = readAllFilesAndDirectories($open);
        FileAndDirPrettyDisplay($filesAndDirectories, $dirName);
        closedir($open);
}

function checkIfDirectoryExists($toCheck){
    $open = opendir($GLOBALS['defaultPath']);
    $allFilesAndDirectoriesArray = readAllFilesAndDirectories($open);
    $count = count($allFilesAndDirectoriesArray);
        for($i=0;$i<$count;$i++){
            $type = filetype($GLOBALS['defaultPath']. $allFilesAndDirectoriesArray[$i]);
            if($type == 'dir' && $allFilesAndDirectoriesArray[$i] == $toCheck){
                return false;
            }
        }
    closedir($open);
        return true;
}

function displayMessageAfterSubmittion($urlValue, $msg){
    if(isset($_GET[$urlValue]) && $_GET[$urlValue] != null){
        echo "<h6>" .$msg. "</h6>";
    }
}

?>