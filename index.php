<?php
require_once __DIR__ . '/init.php';

    if(isset($_GET['dirname'])){
        $dirPath = $GLOBALS['defaultPath'] . htmlspecialchars($_GET['dirname'], ENT_QUOTES) . '/';
        $dirName = htmlspecialchars($_GET['dirname'], ENT_QUOTES);
        scanDirectory($twig, $dirPath);
    }else if(!isset($_GET['dirname'])){
        scanDirectory($twig, $GLOBALS['defaultPath']);
    }
    echo "<hr>";
    displayUploadForm($dirName);
    require_once 'views/mkdirForm.html';

    displayMessageAfterSubmittion('emptyCreateDirName', 'Please enter a Directory name in order to create one.duh');
    displayMessageAfterSubmittion('directoryExists', 'Directory with entered name already exists!');
    displayMessageAfterSubmittion('directoryCreationFailed', 'Directory creation failed.Server Error');
    displayMessageAfterSubmittion('directoryCreated', 'Directory Successfully created!');

    ?>

