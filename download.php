<?php
require_once 'init.php';
    if(!isset($_GET['filename']) || $_GET['filename'] == null){
        header('location:index.php');
        exit();
    }

    $theFile = $_GET['filename'];
    $theFile = htmlspecialchars($theFile);
    $theFile = trim($theFile);

    $theDir = $GLOBALS['defaultPath'];
    if($_GET['dirname'] != 'uploads'){
        $theDir .= $_GET['dirname'] . '/';
    }
    $theDir = htmlspecialchars($theDir);
    $theDir = trim($theDir);



    $fullPathToTheFile = $theDir . $theFile;
    if(file_exists($fullPathToTheFile)){
        $basename = basename($fullPathToTheFile);
        $length   = sprintf("%u", filesize($fullPathToTheFile));

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $basename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $length);

        set_time_limit(0);
        readfile($fullPathToTheFile);
    }