<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once  __DIR__ .  '/functions.php';
$twig = twigInit();

// default values
$GLOBALS['defaultPath'] = '/var/uploads/';
$GLOBALS['uploadDirBase'] = '/var/';
$GLOBALS['dirName'] = 'uploads';

?>